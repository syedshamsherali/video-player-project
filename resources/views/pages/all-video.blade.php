@extends('layout.app')
@section('title','Videos')
@section('content')
<main role="main">
    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                @if(count($videos) > 0)
                    @foreach($videos as $video)
                        <div class="col-md-4">
                            <a href="{{route('player-page',$video->id)}}">
                               <div class="card mb-4 box-shadow">
                                   <video width="320" height="195" class="card-img-top" controls>
                                       <source src="{{asset('assets/upload/'.$video->video)}}" type="video/mp4">
                                   </video>
                                   <div class="card-body">
                                        <p class="card-text">{{$video->title}}</p>
                                   </div>
                               </div>
                            </a>
                        </div>
                    @endforeach
                @else
                <h4>No Videos are available</h4>
                @endif
            </div>
        </div>
    </div>
</main>
@endsection
