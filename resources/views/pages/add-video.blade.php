@extends('layout.app')
@section('title','Add Video')
@section('style')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('assets/css/dropzone.css')}}" >
    <link rel="stylesheet" href="{{asset('assets/css/basic.css')}}" >
@endsection
@section('content')
    <div class="container mt-5">
        <h2>Add New Video</h2>
        <p>Note <code>.Video Should Be Less Then 20MB</code></p>
        <form action="{{route('save-video-details')}}" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
          @csrf
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" class="form-control" id="title" placeholder="Enter Title" name="title" required>
                <div class="valid-feedback">Valid.</div>
                <div class="invalid-feedback">Please fill out this field.</div>
            </div>
            <div class="form-group">
                <label for="video">Select Video:</label>
                <div class="dropzone" id="myDropzone"></div>
                <input type="hidden" name="video" id="video-name">
                <div class="valid-feedback">Valid.</div>
                <div class="invalid-feedback">Please select video file.</div>
            </div>
            <div class="form-group">
                <label for="desc">Description:</label>
                <textarea  class="form-control" id="desc" name="description" required cols="20" rows="5"></textarea>
                <div class="valid-feedback">Valid.</div>
                <div class="invalid-feedback">Please fill out this field.</div>
            </div>
            <div id="dropzone">

            </div>
            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script>
        // Disable form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    <script src="{{asset('assets/js/dropzone.js')}}"></script>
    <script>
        Dropzone.options.myDropzone= {
            url: '{{route("upload-video")}}',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            autoProcessQueue: true,
            uploadMultiple:true,
            maxFilesize: 20,
            maxFiles:1,
            acceptedFiles: 'video/*',
            addRemoveLinks: true,
            success:function (responseText){
                var fileName = JSON.parse(responseText.xhr.response);
                $('#video-name').val(fileName['file_name'])
            }
        }
    </script>

    <script>
        @if(session()->has('success'))
        swal('Great!','{{session()->get('status')}}','success');
        {{session()->forget('status')}}
        @endif

        @if(session()->has('error'))
        swal('missing!','{{session()->get('error')}}','error');
        {{session()->forget('error')}}
        @endif
    </script>
@endsection
