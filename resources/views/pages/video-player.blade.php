@extends('layout.app')
@section('title','Video Player')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/global.css')}}">
    <script src="{{asset('assets/js/FWDUVPlayer.js')}}"></script>

    <script type="text/javascript">
        FWDUVPUtils.onReady(function(){

            new FWDUVPlayer({

                //main settings
                instanceName:"player1",
                parentId:"myDiv",
                playlistsId:"playlists",
                mainFolderPath:"{{asset('assets/player/content')}}",
                skinPath:"minimal_skin_dark",
                displayType:"responsive",
                youtubeAPIKey:"",
                initializeOnlyWhenVisible:"no",
                useVectorIcons:"no",
                fillEntireVideoScreen:"no",
                goFullScreenOnButtonPlay:"no",
                playsinline:"yes",
                useHEXColorsForSkin:"no",
                normalHEXButtonsColor:"#FF0000",
                googleAnalyticsTrackingCode:"",
                useResumeOnPlay:"no",
                useDeepLinking:"yes",
                addKeyboardSupport:"yes",
                showPreloader:"yes",
                preloaderBackgroundColor:"#000000",
                preloaderFillColor:"#FFFFFF",
                autoScale:"yes",
                showButtonsToolTip:"yes",
                stopVideoWhenPlayComplete:"no",
                playAfterVideoStop:"no",
                autoPlay:"no",
                loop:"no",
                shuffle:"no",
                showErrorInfo:"yes",
                maxWidth:980,
                maxHeight:552,
                buttonsToolTipHideDelay:1.5,
                volume:.8,
                backgroundColor:"#000000",
                videoBackgroundColor:"#000000",
                posterBackgroundColor:"#000000",
                buttonsToolTipFontColor:"#5a5a5a",
                //logo settings
                showLogo:"yes",
                hideLogoWithController:"yes",
                logoPosition:"topRight",
                logoLink:"http://www.webdesign-flash.ro/",
                logoMargins:10,
                //playlists/categories settings
                showPlaylistsSearchInput:"yes",
                usePlaylistsSelectBox:"yes",
                showPlaylistsButtonAndPlaylists:"yes",
                showPlaylistsByDefault:"no",
                thumbnailSelectedType:"opacity",
                startAtPlaylist:0,
                buttonsMargins:0,
                thumbnailMaxWidth:350,
                thumbnailMaxHeight:350,
                horizontalSpaceBetweenThumbnails:40,
                verticalSpaceBetweenThumbnails:40,
                inputBackgroundColor:"#333333",
                inputColor:"#999999",
                //playlist settings
                showPlaylistButtonAndPlaylist:"no",
                playlistPosition:"right",
                showPlaylistByDefault:"yes",
                showPlaylistName:"yes",
                showSearchInput:"yes",
                showLoopButton:"yes",
                showShuffleButton:"yes",
                showNextAndPrevButtons:"yes",
                showThumbnail:"yes",
                forceDisableDownloadButtonForFolder:"yes",
                addMouseWheelSupport:"yes",
                startAtRandomVideo:"no",
                stopAfterLastVideoHasPlayed:"no",
                randomizePlaylist:'no',
                folderVideoLabel:"VIDEO ",
                playlistRightWidth:310,
                playlistBottomHeight:599,
                startAtVideo:0,
                maxPlaylistItems:50,
                thumbnailWidth:70,
                thumbnailHeight:70,
                spaceBetweenControllerAndPlaylist:1,
                spaceBetweenThumbnails:2,
                scrollbarOffestWidth:8,
                scollbarSpeedSensitivity:.5,
                playlistBackgroundColor:"#000000",
                playlistNameColor:"#FFFFFF",
                thumbnailNormalBackgroundColor:"#1b1b1b",
                thumbnailHoverBackgroundColor:"#313131",
                thumbnailDisabledBackgroundColor:"#272727",
                searchInputBackgroundColor:"#000000",
                searchInputColor:"#999999",
                youtubeAndFolderVideoTitleColor:"#FFFFFF",
                folderAudioSecondTitleColor:"#999999",
                youtubeOwnerColor:"#888888",
                youtubeDescriptionColor:"#888888",
                mainSelectorBackgroundSelectedColor:"#FFFFFF",
                mainSelectorTextNormalColor:"#FFFFFF",
                mainSelectorTextSelectedColor:"#000000",
                mainButtonBackgroundNormalColor:"#212021",
                mainButtonBackgroundSelectedColor:"#FFFFFF",
                mainButtonTextNormalColor:"#FFFFFF",
                mainButtonTextSelectedColor:"#000000",
                //controller settings
                showController:"yes",
                showControllerWhenVideoIsStopped:"yes",
                showNextAndPrevButtonsInController:"no",
                showRewindButton:"yes",
                showPlaybackRateButton:"yes",
                showVolumeButton:"yes",
                showTime:"yes",
                showQualityButton:"yes",
                showInfoButton:"yes",
                showDownloadButton:"yes",
                showFacebookButton:"yes",
                showEmbedButton:"yes",
                showChromecastButton:"no",
                showFullScreenButton:"yes",
                disableVideoScrubber:"no",
                showScrubberWhenControllerIsHidden:"yes",
                showDefaultControllerForVimeo:"no",
                showMainScrubberToolTipLabel:"yes",
                repeatBackground:"yes",
                controllerHeight:37,
                controllerHideDelay:3,
                startSpaceBetweenButtons:7,
                spaceBetweenButtons:8,
                scrubbersOffsetWidth:2,
                mainScrubberOffestTop:14,
                timeOffsetLeftWidth:5,
                timeOffsetRightWidth:3,
                timeOffsetTop:0,
                volumeScrubberHeight:80,
                volumeScrubberOfsetHeight:12,
                timeColor:"#888888",
                youtubeQualityButtonNormalColor:"#888888",
                youtubeQualityButtonSelectedColor:"#FFFFFF",
                scrubbersToolTipLabelBackgroundColor:"#FFFFFF",
                scrubbersToolTipLabelFontColor:"#5a5a5a",
                //advertisement on pause window
                aopwTitle:"Advertisement",
                aopwWidth:400,
                aopwHeight:240,
                aopwBorderSize:6,
                aopwTitleColor:"#FFFFFF",
                //subtitle
                subtitlesOffLabel:"Subtitle off",
                //popup add windows
                showPopupAdsCloseButton:"yes",
                //embed window and info window
                embedAndInfoWindowCloseButtonMargins:15,
                borderColor:"#333333",
                mainLabelsColor:"#FFFFFF",
                secondaryLabelsColor:"#a1a1a1",
                shareAndEmbedTextColor:"#5a5a5a",
                inputBackgroundColor:"#000000",
                inputColor:"#FFFFFF",
                //audio visualizer
                audioVisualizerLinesColor:"#0099FF",
                audioVisualizerCircleColor:"#FFFFFF",
                //lightbox settings
                closeLightBoxWhenPlayComplete:"no",
                lightBoxBackgroundOpacity:.6,
                lightBoxBackgroundColor:"#000000",
                //sticky on scroll
                stickyOnScroll:"no",
                stickyOnScrollShowOpener:"yes",
                stickyOnScrollWidth:"700",
                stickyOnScrollHeight:"394",
                //sticky display settings
                showOpener:"yes",
                showOpenerPlayPauseButton:"yes",
                verticalPosition:"bottom",
                horizontalPosition:"center",
                showPlayerByDefault:"yes",
                animatePlayer:"yes",
                openerAlignment:"right",
                mainBackgroundImagePath:"{{asset('assets/css/minimal_skin_dark/main-background.png')}}",
                openerEqulizerOffsetTop:-1,
                openerEqulizerOffsetLeft:3,
                offsetX:0,
                offsetY:0,
                //loggin
                isLoggedIn:"no",
                playVideoOnlyWhenLoggedIn:"yes",
                loggedInMessage:"Please login to view this video.",
                //playback rate / speed
                defaultPlaybackRate:1, //0.25, 0.5, 1, 1.25, 1.2, 2
                //cuepoints
                executeCuepointsOnlyOnce:"no",
                //ads
                openNewPageAtTheEndOfTheAds:"no",
                playAdsOnlyOnce:"no",
                adsButtonsPosition:"left",
                skipToVideoText:"You can skip to video in: ",
                skipToVideoButtonText:"Skip Ad",
                adsTextNormalColor:"#888888",
                adsTextSelectedColor:"#FFFFFF",
                adsBorderNormalColor:"#666666",
                adsBorderSelectedColor:"#FFFFFF",
                //a to b loop
                useAToB:"yes",
                atbTimeBackgroundColor:"transparent",
                atbTimeTextColorNormal:"#888888",
                atbTimeTextColorSelected:"#000000",
                atbButtonTextNormalColor:"#FFFFFF",
                atbButtonTextSelectedColor:"#FFFFFF",
                atbButtonBackgroundNormalColor:"#888888",
                atbButtonBackgroundSelectedColor:"#000000",
                // context menu
                showContextmenu:'yes',
                showScriptDeveloper:"no",
                contextMenuBackgroundColor:"#1f1f1f",
                contextMenuBorderColor:"#1f1f1f",
                contextMenuSpacerColor:"#333",
                contextMenuItemNormalColor:"#888888",
                contextMenuItemSelectedColor:"#FFFFFF",
                contextMenuItemDisabledColor:"#444"
            });
        });
    </script>
@endsection
@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="offset-md-2"></div>
            <div class="col-md-8">
                <div id="myDiv">
                    <ul id="playlists" style="display:none;">
                        <li data-source="playlists" data-thumbnail-path="{{asset('assets/upload/'.$video->video)}}" data-thumb-source="{{asset('assets/player/')}}/content/thumbnails/small-fwd.jpg" data-video-source="[{source:'{{asset('assets/upload/'.$video->video)}}', label:'small version'}, {source:'{{asset('assets/upload/'.$video->video)}}', label:'hd720'},{source:'{{asset('assets/upload/'.$video->video)}}', label:'hd1080'}]" data-start-at-video="2" data-poster-source="{{asset('assets/upload/'.$video->video)}}"  data-downloadable="yes">
                            <div data-video-short-description="">
                                <div>
                                    <p class="fwduvp-thumbnail-title">{{$video->title}}</p>
                                    <p class="fwduvp-thumbnail-description">{{$video->description}}.</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endsection
