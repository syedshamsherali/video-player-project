<?php

use App\Http\Controllers\VideoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[VideoController::class,'index'])->name('index');
Route::get('/add-new-video', [VideoController::class,'add_video_form'])->name('add-video');
Route::post('/save-video-details', [VideoController::class,'create'])->name('save-video-details');
Route::post('/upload-video', [VideoController::class,'upload_video'])->name('upload-video');
Route::get('/player-page/{id}', [VideoController::class,'player_page'])->name('player-page');
