<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;
    protected $fillable = ['title','video','description'];

    public function save_video($array = Array()){
        $this->create([
            'title'=>$array['title'],
            'video'=>$array['video'],
            'description'=>$array['description'],
        ]);
    }
}
