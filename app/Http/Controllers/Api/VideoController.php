<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{

    public function upload_video(Request $request){
        $validate = Validator::make($request->all(), [
            'video'=>'required|max:20000',
            'title'=>'required',
            'description'=>'required',
        ]);

        if($validate->fails()){
            return response()->json(['message'=>'missing parameters','status'=>404,'errors'=>$validate->errors()],404);
        }

        // Handle File Upload
        if($request->hasFile('video')) {
            // Get filename with extension
            $file = $request->file('video');
            // Get just filename
            $fileName =  time().$file->getClientOriginalName();
            // Get just ext
            //Filename to store
            $fileStorePath = public_path('assets/upload');
            // Upload Image
            $file->move($fileStorePath, $fileName);
        }
        Video::create([
            'title'=>$request['title'],
            'video'=>$fileName,
            'description'=>$request['description'],
        ]);

            return response()->json(['message'=>'file uploaded successfully','status'=>200,'file_name'=>$fileName],200);
    }
}
