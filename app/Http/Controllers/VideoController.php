<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class VideoController extends Controller
{
    public function index()
    {
        $videos = Video::all()->sortByDesc('id');
        return view('pages.all-video',compact('videos'));
    }

    public function add_video_form()
    {
        return view('pages.add-video');
    }

    public function create(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'title'=>'required',
            'video'=>'required',
            'description'=>'required'
        ]);

        if($validate->fails()){
            session()->put('error',json_encode($validate->errors()->messages()));
            return redirect()->back();
        }

        Video::create([
           'title'=>$request->title,
           'video'=>$request->video,
           'description'=>$request->description,
        ]);
        session()->put('success','Video Added Successfully');

        return redirect()->back();
    }

    public function upload_video(Request $request){
        $this->validate($request, [
            'file'=>'required|max:20000',
        ]);
        // Handle File Upload
        if($request->hasFile('file')) {
            // Get filename with extension
            $file = $request->file('file')[0];
            // Get just filename
            $fileName =  time().$file->getClientOriginalName();
            // Get just ext
            //Filename to store
            $fileStorePath = public_path('assets/upload');
            // Upload Image
            $file->move($fileStorePath, $fileName);
        }

        return response()->json(['message'=>'success','status'=>200,'file_name'=>$fileName],200);
    }

    public function player_page($id){
        $video = Video::find($id);
        if(!$video){
            session()->put('error','video not found ');
            return redirect()->back();
        }
        return view('pages.video-player',compact('video'));
    }
}
